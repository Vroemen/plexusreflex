function rotatePics(currentPhoto) {
  var numberOfPhotos = $('#fotoslider img').length;
  currentPhoto = currentPhoto % numberOfPhotos;
	
  $('#fotoslider img').eq(currentPhoto).fadeOut(function() {
		// re-order the z-index
    $('#fotoslider img').each(function(i) {
      $(this).css(
        'zIndex', ((numberOfPhotos - i) + currentPhoto) % numberOfPhotos
      );
    });
    $(this).show();
    setTimeout(function() {rotatePics(++currentPhoto);}, 5000);
  });
}
