<!DOCTYPE html>
<!-- !DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<html>
<?xml version="1.0" encoding="iso-8859-1"?>
<head>
<link rel="stylesheet" href="css/reset.css" /> 
<link rel="stylesheet" href="css/style.css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link href='http://fonts.googleapis.com/css?family=Gilda+Display' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
<meta name="keywords" content="Plexus, reflexologie Limburg, reflexologie Maastricht, voetreflexologie, Therapeut, Dorntherapie, Maastricht" />
<meta name="description" content="Plexus kan u helpen uw lichaam en geest in balans te brengen waardoor klachten zoals stress, maag- en darmklachten, overspannenheid etc.  kunnen worden voorkomen of verlicht." />
<meta name="revisit-after" content="7 days" />


    <title>Contact</title>
</head>
<!--[if IE ]>
<body class="ie">
<![endif]-->
<!--[if !IE]>-->
<body>
<!--<![endif]-->

<div id="container">
    <div id="hoofd">
        <div class="navigation">
            <ul>
                <li><a href="index.html" >home</a></li>
                <li><a href="behandelingen.html">Behandelingen</a></li>
                <li><a href="verzekeraars.html">Verzekeraars</a></li>
                <li><a href="contact.php" class="current">Contact</a></li>
            </ul>
        </div>
        <div class="logo"><a href="index.html"><img src="images/plexuslogo.png" alt="plexuslogo" /></a>
        </div>
    </div>
    <div id="main">
        <div id="links">
            <div id = "mailformulier">
            <form action="" method="post">
                <?php if (isset($status)) : ?>
                <p class = "notice"><?php echo $status; ?></p>
            <h2>Wilt u contact opnemen of heeft u een vraag?</h2><br />
            <p>Stuur ons uw boodschap via onderstaand contactformulier en ik neem zo spoedig mogelijk contact met u op, alvast bedankt!</p><br />
            <p>Voornaam:</p>
            <input name="voornaam" class="invoer" type="text" size="25" value="<?= old('voornaam'); ?>" <br>
                <p>Achternaam:</p>
            <input name="achternaam" class="invoer" type="text" size="25" value="<?= old('achternaam'); ?>"><br><br>
                <p>Email-adres:</p>
                <input name="email" class="invoer" type="text" size="50" value="<?= old('email'); ?>"><br><br>
                <p>Hoe heeft u ons gevonden? <br /></p>
                <select name="hoegevonden" class="invoer">
                        <option value="" > -- aub kiezen -- </option>
                        <option>Google/zoekmachine</option>
                        <option>Verwijzing vanuit andere website</option>
                        <option>Reclame in wijkblad</option>
                        <option>Mond aan mond reclame</option>
                        <option>Anders</option>
                    </select><br /><br />
                <p>Uw vraag/opmerking:</p>
                <textarea name="boodschap" class="invoer" cols="50" rows="10" value="<?= old('boodschap'); ?>" ></textarea><br><br>
                <button>Verstuur</button>
            </form>
            </div>
    </div>

        <div id="rechts">
            <h2>Lokatie praktijk Plexus</h2><br />
            <!-- Google maps kaart -->
<!--            <iframe width="480" height="360" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.nl/maps?f=q&amp;source=s_q&amp;hl=nl&amp;geocode=&amp;q=Cantecleerstraat+108,+Maastricht&amp;aq=&amp;sll=50.857645,5.662504&amp;sspn=0.011134,0.033023&amp;ie=UTF8&amp;hq=&amp;hnear=Cantecleerstraat+108,+6217+BX+Maastricht,+Limburg&amp;t=m&amp;ll=50.857137,5.661778&amp;spn=0.013003,0.027466&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.nl/maps?f=q&amp;source=embed&amp;hl=nl&amp;geocode=&amp;q=Cantecleerstraat+108,+Maastricht&amp;aq=&amp;sll=50.857645,5.662504&amp;sspn=0.011134,0.033023&amp;ie=UTF8&amp;hq=&amp;hnear=Cantecleerstraat+108,+6217+BX+Maastricht,+Limburg&amp;t=m&amp;ll=50.857137,5.661778&amp;spn=0.013003,0.027466&amp;z=15&amp;iwloc=A" style="color:#0000FF;text-align:left"><br />Grotere kaart weergeven</a></small>-->
            <iframe width="400" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.nl/maps?f=q&amp;source=embed&amp;hl=nl&amp;geocode=&amp;q=Cantecleerstraat+108,+Maastricht&amp;aq=&amp;sll=50.857645,5.662504&amp;sspn=0.011134,0.033023&amp;ie=UTF8&amp;hq=&amp;hnear=Cantecleerstraat+108,+6217+BX+Maastricht,+Limburg&amp;t=m&amp;ll=50.85646,5.661263&amp;spn=0.004741,0.00912&amp;z=16&amp;output=embed"></iframe><br /><small><a href="https://maps.google.nl/maps?f=q&amp;source=embed&amp;hl=nl&amp;geocode=&amp;q=Cantecleerstraat+108,+Maastricht&amp;aq=&amp;sll=50.857645,5.662504&amp;sspn=0.011134,0.033023&amp;ie=UTF8&amp;hq=&amp;hnear=Cantecleerstraat+108,+6217+BX+Maastricht,+Limburg&amp;t=m&amp;ll=50.85646,5.661263&amp;spn=0.004741,0.00912&amp;z=16" style="color:#0000FF;text-align:left"><br />Grotere kaart weergeven</a></small>
            <!-- Google maps kaart -->
            <br />
            <p></p>
        </div>
    </div>
    <div id="footer">
        <div id= "balkfooter" class="balk">
            <div id = "footerleft">
                <h2><a href="index.html">Plexus</a></h2><br />
                <p>Mevrouw J. Vroemen</p><br />
                <p>Cantecleerstraat 108</p><br />
                <p>6217 BX Maastricht</p><br />
                <p>T: +31 6 22 67 74 54</p><br />
                <P>E: Jomima@Plexusreflex.nl</P>
            </div>
        </div>
    </div>
</div>


</body>
</html>