<?php
require 'functions.php';

/* Check alle formulier inputvelden mbv check_input function */
if ($_SERVER['REQUEST_METHOD']== 'POST'){
    $voornaam = trim($_POST['voornaam']);
    $achternaam = trim($_POST['achternaam']);
    $hoegevonden = trim($_POST['hoegevonden']);
    $email = trim($_POST['email']);
    $boodschap = trim($_POST['boodschap']);

    if(empty($voornaam){
        $status = 'Voornaam invullen aub.';
    } else {
        if(empty($achternaam){
        $status = 'Achternaam invullen aub.';
        } else {
            if(!valid($email){
                $status = 'Email-adres is niet correct, korrigeren aub.';
            } else {
                send_email($voornaam, $achternaam, $hoegevonden, $email, $boodschap);
                $status = 'Bedankt voor uw bericht, gegevens zijn doorgestuurd';
            }
        }
    }

}
<p>test</p>
// require 'contact.tmpl.php';
?>